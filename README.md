# facebook/webdriver 

Client for Selenium WebDriver. https://packagist.org/packages/facebook/webdriver

[![PHPPackages Rank](http://phppackages.org/p/facebook/webdriver/badge/rank.svg)](http://phppackages.org/p/facebook/webdriver)
[![PHPPackages Referenced By](http://phppackages.org/p/facebook/webdriver/badge/referenced-by.svg)](http://phppackages.org/p/facebook/webdriver)

* Inspires https://gitlab.com/php-packages-demo/api-platform-api-pack